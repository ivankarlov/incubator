/*jslint browser: true*/
/*jslint nomen: true*/
/*global $, jQuery, map, initialize, moment, alert, console, google, enquire, WebFont, WebFontConfig: true*/
$.fn.extend({
  toggleText: function (a, b) {
    "use strict";
    var that = this;
    if (that.text() !== a && that.text() !== b) {
      that.text(a);
    } else if (that.text() === a) {
      that.text(b);
    } else if (that.text() === b) {
      that.text(a);
    }
    return this;
  }
});

$(function () {
  'use strict';

  //Detect opera mini
  if (/Opera Mini/.test(navigator.userAgent)) {
    document.getElementsByTagName('html')[0].className += ' operamini';
  }

  //Detect mobile devices
  if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
    document.getElementsByTagName('html')[0].className += ' mobile';
  }

  if ($(".fancybox").length) {
    $(".fancybox").fancybox({
      padding: 0,
      helpers: {
        overlay: {
          locked: false // important!!
        }
      }
    });
  }

  //Popup
  $('.popup-link').on('click', function (e) {
    e.preventDefault();
    var a = $(this).attr('href');
    $(a).bPopup({
      opacity: 0.7,
      transition: 'fadeIn',
      onOpen: function () {
        $(this).attr('aria-hidden', false);
        $('select, input:checkbox, input:radio').trigger('refresh');
      },
      onClose: function () {
        $(this).attr('aria-hidden', true);
      }
    });
  });

  $('.puzzle-slider').slick({
    infinite: false
  });

  $('.bi-calendar-slider').slick({
    infinite: false,
    slidesToShow: 11
  });

  $('.bi-partners-slider').slick({
    infinite: false,
    slidesToShow: 6,
    slidesToScroll: 6
  });

  $('.bi-present-slider').slick({
    infinite: false,
    slidesToShow: 3,
    slidesToScroll: 3
  });

  $('.bi-nav-search-link').on('click', function () {
    $(this).next().slideToggle();
  });

  $('select, :checkbox, :radio').styler();

  if (typeof WebFont !== 'undefined') {
    WebFontConfig = {
      custom: {
        families: ['Ubuntu']
      },
      active: function () {
        $('select, :checkbox, :radio').trigger('refresh');
      }
    };
    WebFont.load(WebFontConfig);
  }


  $(".left-navigation-scroll-inner").mCustomScrollbar({
    theme: "minimal-dark"
  });

  $('.left-navigation-toggler').on('click', function () {
    $('#left-navigation').toggleClass("open");
  });

  enquire.register("screen and (max-width: 95.9375em)", {
    match: function () {
      $('#left-navigation').removeClass("open");
    },
    unmatch: function () {
      $('#left-navigation').addClass("open");
    },
    setup: function () {
      $('#left-navigation').removeClass("open");
    }
  });

  $('.bi-state-support-column').each(function () {
    $(this).find('.bi-state-support-column-sub-types').each(function (i) {
      $(this).css('visibility', 'hidden').delay((i = i + 1) * 100).queue(function () {
        $(this).css('visibility', 'visible').addClass('animated fadeInDown');
        $(this).dequeue();
      });
    });
  });

  $('.bi-office-view-slider').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    fade: true,
    arrows: false,
    asNavFor: '.bi-office-preview-slider'
  });

  $('.bi-office-preview-slider').slick({
    slidesToShow: 3,
    slidesToScroll: 3,
    asNavFor: '.bi-office-view-slider',
    focusOnSelect: true
  });

  $('.bi-album-view-slider').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    fade: true,
    asNavFor: '.bi-album-preview-slider',
    infinite: false
  });

  $('.bi-album-preview-slider').slick({
    slidesToShow: 5,
    slidesToScroll: 5,
    asNavFor: '.bi-album-view-slider',
    focusOnSelect: true,
    arrows: false,
    infinite: false
  });

  $('.fade-box-button').on('click', function () {
    $(this).parents('.fade-box').fadeOut(function () {
      $(this).siblings('.fade-box').fadeIn();
    });
  });

  var settings = {
      placement: 'auto-bottom',
      animation: 'pop'
    },
    largeContent = $('#largeContent').html(),
    largeSettings = {
      content: largeContent,
      width: 295,
      height: 400,
      delay: {
        show: 300,
        hide: 1000
      }
    },
    popLarge = $('.popover').webuiPopover($.extend({}, settings, largeSettings)).on('shown.webui.popover', function (e) {
      $('.webui-popover-content').mCustomScrollbar({
        theme: "minimal-dark"
      });
    });

  setTimeout(function () {
    $('.bi-startup-stage-item').equalHeight();
    $('h3.bi-chain-link-caption').equalHeight();
  }, 100);

  $('.input-daterange').datepicker({
    autoclose: true,
    todayHighlight: true,
    language: 'ru',
    orientation: 'auto bottom'
  });

  $('.bi-calendar-event-content-topic').expander({
    slicePoint: 520,
    expandText: 'Подробнее',
    userCollapseText: 'Свернуть',
    expandEffect: 'fadeIn',
    collapseEffect: 'fadeOut'
  });

  var $container_height;

  $('.toggler').on('click', function () {

    var $box = $(this).parent().prev(),
      $inner_height = $box.children().outerHeight(true);

    if (!$box.hasClass('expanded')) {
      $(this).attr('aria-expanded', true).addClass('active').text('Скрыть');
      $container_height = $box.outerHeight(true);
      $box.animate({
        'height': $inner_height
      });
      $box.addClass('expanded');
    } else {
      $(this).attr('aria-expanded', false).removeClass('active').text('Вся программа');
      $box.removeClass('expanded');
      $box.animate({
        'height': $container_height
      });
    }
  });

});


$(window).on('scroll', function () {
  'use strict';
  if ($(window).scrollTop() >= ($('body').height() - $('.bi-fullscreen-box #navigation').height())) {
    $('.bi-fullscreen-box #navigation').addClass('fixed');
    $('.bi-fullscreen-box .nav-logo').addClass('visible');
  } else {
    $('.bi-fullscreen-box #navigation').removeClass('fixed');
    $('.bi-fullscreen-box .nav-logo').removeClass('visible');
  }
});